import { useState } from "react";

import Cart from "./components/Cart/Cart";
import Header from "./components/Layout/Header";
import Meals from "./components/Meals/Meals";
import CartProvider from "./store/CartProvider";

const App = () => {
  const [showCart, setShowCart] = useState(false);

  const showCartHandler = () => {
    setShowCart(true);
  }

  const closeCartHandler = () => {
    setShowCart(false);
  }

  return (
    <CartProvider>
      <Header onShowCartClick={showCartHandler} />
      <main>
        <Meals />
      </main>
      {showCart && <Cart onClose={closeCartHandler} />}
    </CartProvider>
  );
}

export default App;
