import AvailavbleMeals from './AvailableMeals';
import MealsSummary from './MealsSummary';

const Meals = () => {
    return ( 
        <>
            <MealsSummary />
            <AvailavbleMeals />
        </>
     );
}
 
export default Meals;