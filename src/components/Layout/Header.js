import HeaderCartButton from "./HeaderCartButton";

import MealsImg from "../../assets/meals.jpg";
import classes from './Header.module.css';

const Header = (props) => {

  return (
    <>
      <header className={classes.header}>
        <h1>React Meals</h1>
        <HeaderCartButton onClick={props.onShowCartClick} />
      </header>
      <div className={classes["main-image"]}>
        <img src={MealsImg} alt="A table full of delicious food!" />
      </div>
    </>
  );
};

export default Header;
